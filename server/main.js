function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}
(async () => {
	
	//*
	var httpServer = require('./httpServer');
	httpServer.start();
	
	
	//*/
	
	/*
	/////////////TESTING SOCKETS///////////////////
	httpServer.events.on('getMatches',({socket,msg}) => {
		console.log(socket,msg);
		socket.emit("matches", {data :"testing"});
	});
	do {
		httpServer.broadcast({event: "updates", data: {msg: "test"} });
		await sleep(1000);
	}while(true);
	//*/
	
	//*
	var scrapServer = require('./scrapServer');
	var options = {}
	process.argv.forEach((val, index) => {
		if(val=="-debug") {
			options.debug = true;
		}
		if(val=="-random") {
			options.demo = true;
		}
	  });
	scrapServer.start(options);
	scrapServer.events.on('debug', (msg) => console.log(msg));
	scrapServer.events.on('change', function (data) { 
		httpServer.broadcast({event: "updates", data : {msg: data} });
	});	
	
	httpServer.events.on('getMatches',({socket,msg}) => {
		socket.emit("matches", {...scrapServer.matches});
	});
	
	//*/
})();
