const events = require('events'),
	express = require('express'),
	cors = require('cors'),
	path = require('path'),
	fs = require('fs'),
	httpProtocol = require('http'),
	socketIo = require('socket.io');

var domainPath = function (req, res, next) {
  req.originalUrl = "/index.html"
  next();
}

var clientPath = function(request, response,next){
	try {
		var requestUrl = request.originalUrl
		// need to use path.normalize so people can't access directories underneath baseDirectory
		var fsPath = __dirname+path.normalize(requestUrl)
		console.log(fsPath);
		var fileStream = fs.createReadStream(fsPath)
		fileStream.pipe(response)
		fileStream.on('open', function() {
			 response.writeHead(200)
		})
		fileStream.on('error',function(e) {
			 //response.writeHead(404)     // assume the file doesn't exist
			 //response.end()
		})
	} catch(e) {
		//response.writeHead(500)
		//response.end()     // end the response so browsers don't hang
		console.log(e.stack)
	}
	next()
}

module.exports = {
	events : new events.EventEmitter(),
	io : null,
	broadcast: function({event, data}) {
		this.io.emit(event, {...data, time : Date()});
	},
	start: function() {
		
		var app = express();
		var http = httpProtocol.Server(app);
		var io = socketIo(http)
		this.io = io;
		app.use(cors({credentials: false, origin: '*'}));
		
		app.use(express.static(__dirname));
		app.get('/', domainPath);
		app.get('/*', clientPath);
		var module = this;
		io.on('connection', function(socket){
			console.log('a user connected');
			socket.on('disconnect', function(){
				console.log('user disconnected');
			});
			socket.on('getMatches', function(msg){
				console.log('user request getMatches');
				module.events.emit('getMatches', {socket,msg})
			});
		});
		
		http.listen(3000, function(){
		  console.log('listening on *:3000');
		});
	}
}
