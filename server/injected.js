function getMatches() {
	let odds = jQuery(".gl-ParticipantOddsOnly_Odds").map(function () { return  jQuery(this).text()}).get();
	
	let leagues = jQuery(".gl-MarketGroup").map(function () { 
		let leagueElement =  jQuery(this);
		let league = leagueElement.find(".cm-CouponMarketGroupButton_Title").text();
		var matches = leagueElement.find(".sl-CouponParticipantWithBookCloses_NameContainer").map(function () { 
			return {name : jQuery(this).text()};
		}).get();
		let markets = leagueElement.find(".gl-MarketColumnHeader:not(.sl-MarketHeaderLabel_Date)").map(function () { 
			let market = jQuery(this).text()
			let odds = jQuery(this).nextUntil(".gl-MarketColumnHeader", ".gl-ParticipantOddsOnly").map(function () { 
				return jQuery(this).text();
			}).get();
			return {market , odds};
		}).get();
		let machesRefactored = {}
		for (matchIndex in matches) {
			let oddsRefactored = {}
			for (marcketIndex in markets) {
				if(markets[marcketIndex].market != " ") {
					oddsRefactored[markets[marcketIndex].market] = markets[marcketIndex].odds[matchIndex];
				}
			}
			machesRefactored[matches[matchIndex].name] = oddsRefactored;
		}
		return {
			 league, 
			 maches : machesRefactored
		 };
	}).get();
	
	let leagueRefactored = {}
	for (leagueIndex in leagues) {
		leagueRefactored[leagues[leagueIndex].league] = leagues[leagueIndex].maches
	}
	return leagueRefactored;
}
