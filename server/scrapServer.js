const puppeteer = require('puppeteer');
const events = require('events');

const escapeXpathString = str => {
  const splitedQuotes = str.replace(/'/g, `', "'", '`);
  return `concat('${splitedQuotes}', '')`;
};
function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
};
module.exports = {
	browser : null,
	matches : null,
	events : new events.EventEmitter(),
	clickByText : async function(page, text, retries = 30) {
		this.events.emit('debug', {text, retries});
		try {
			const escapedText = escapeXpathString(text);
			const linkHandlers = await page.$x(`//div[contains(text(), ${escapedText})]`);

			if (linkHandlers.length > 0) {
				await linkHandlers[linkHandlers.length - 1].click();
			} else {
				
				throw new Error(`Link not found: ${text}`);
			}
			return 0;
		}
		catch(err) {
			if(retries > 0) {
			  await page.waitFor(1*1000);
			  return this.clickByText(page, text, retries - 1);
			}
			this.events.emit('debug', err);
			return -1;
		}
	},
	getRandom : function(leagues) {
		var newData = {}
		for (league in leagues) {
			var newLeague = {};
			let matches = leagues[league]
			for (match in matches) {
				var newMatches = {};
				let markets = matches[match]
				for (market in markets) {
					let odds = markets[market]
					var newOdds = odds * (0.95 + Math.random()*0.1);
					newMatches[market] = newOdds;
				}
				newLeague[match] = newMatches;
			}
			newData[league] = newLeague;
		}
		return newData;
	},
	calculateChanges : function(leagues , oldleagues) {
		var changes = []
		for (league in leagues) {
			let matches = leagues[league]
			for (match in matches) {
				let markets = matches[match]
				for (market in markets) {
					let odds = markets[market]
					var oldsOdd = null;
					try {
						oldsOdd = oldleagues[league][match][market];
					}
					catch(e){
					}
					if(odds !== oldsOdd) {
						changes.push({league,match,market,odds,oldsOdd})
					}
				}
			}
		}
		return changes;
	},
	start: async function({demo = false, debug=false}) {
		if(demo) {
			var lastMatches = {"Αγγλία - Πρέμιερ Λιγκ":{"Γουότφορντ v Κρίσταλ Πάλας":{"1":"5.25","2":"1.61","X":"4.00"},"Νιούκαστλ v Τσέλσι":{"1":"5.00","2":"1.61","X":"4.00"},"Φούλαμ v Μπέρνλι":{"1":"5.25","2":"1.70","X":"3.50"},"Μάντσεστερ Γιουν. v Τότεναμ":{},"Λέστερ v Λίβερπουλ":{},"Γουέστ Χαμ v Γουλβς":{},"Έβερτον v Χάντερσφιλντ":{},"Κρίσταλ Πάλας v Σαουθάμπτον":{},"Μπράιτον v Φούλαμ":{},"Τσέλσι v Μπόρνμουθ":{},"Μαν. Σίτι v Νιούκαστλ":{},"Κάρντιφ v Άρσεναλ":{},"Γουότφορντ v Τότεναμ":{},"Μπέρνλι v Μάντσεστερ Γιουν.":{}},"Ισπανία - Πριμέρα Ντιβιζιόν":{"Ατλέτικο Μαδρίτης1-0Ράγιο Βαγεκάνο":{"1":"3.60","2":"2.10","X":"3.25"},"Βαγιαδολίδ v Μπαρτσελόνα":{"1":"2.35","2":"3.30","X":"3.00"},"Εσπανιόλ v Βαλένθια":{"1":"1.083","2":"24.00","X":"10.00"},"Σεβίλλη v Βιγιαρεάλ":{"1":"3.00","2":"2.30","X":"3.40"},"Χιρόνα v Ρεάλ Μαδρίτης":{},"Λεβάντε v Θέλτα":{},"Αθλέτικ Μπιλμπάο v Ουέσκα":{},"Χετάφε v Βαγιαδολίδ":{},"Βιγιαρεάλ v Χιρόνα":{},"Εϊμπάρ v Ρεάλ Σοσιεδάδ":{},"Θέλτα v Ατλέτικο Μαδρίτης":{},"Ράγιο Βαγεκάνο v Αθλέτικ Μπιλμπάο":{},"Ρεάλ Μαδρίτης v Λεγκανιές":{},"Λεβάντε v Βαλένθια":{},"CD Αλαβές v Εσπανιόλ":{},"Μπαρτσελόνα v Ουέσκα":{},"Μπέτις v Σεβίλλη":{}},"Ιταλία - Σέριε Α":{"Νάπολι2-2Μίλαν":{"1":"1.57","2":"5.50","X":"4.00"},"Σπαλ v Πάρμα":{"1":"1.53","2":"6.00","X":"4.00"},"Ίντερ v Τορίνο":{"1":"2.05","2":"3.80","X":"3.20"},"Κάλιαρι v Σασουόλο":{"1":"1.22","2":"13.00","X":"5.75"},"Ουντινέζε v Σαμπντόρια":{"1":"5.50","2":"1.61","X":"3.80"},"Τζένοα v Έμπολι":{"1":"1.95","2":"3.90","X":"3.40"},"Φιορεντίνα v Κιέβο":{"1":"1.66","2":"5.25","X":"3.60"},"Φροζινόνε v Μπολόνια":{},"Ρόμα v Αταλάντα":{},"Μίλαν v Ρόμα":{},"Μπολόνια v Ίντερ":{},"Πάρμα v Γιουβέντους":{},"Φιορεντίνα v Ουντινέζε":{},"Αταλάντα v Κάλιαρι":{},"Κιέβο v Έμπολι":{},"Λάτσιο v Φροζινόνε":{},"Σαμπντόρια v Νάπολι":{},"Σασουόλο v Τζένοα":{},"Τορίνο v Σπαλ":{}},"Γερμανία - Μπουντεσλίγκα Ι":{"Μάιντζ v Στουτγκάρδη":{"1":"1.44","2":"7.50","X":"4.50"},"Ντόρτμουντ v RB Λέιπζιγκ":{"1":"1.75","2":"4.75","X":"3.50"},"Αννόβερο v Ντόρτμουντ":{},"TSG Χόφενχαϊμ v Φράιμπουργκ":{},"Άιντραχτ Φρανκφούρτης v Βέρντερ Βρέμης":{},"Άουγκσμπουργκ v Γκλάντμπαχ":{},"Λεβερκούζεν v Βόλφσμπουργκ":{},"Νυρεμβέργη v Μάιντζ":{},"Στουτγκάρδη v Μπάγερν Μονάχου":{},"RB Λέιπζιγκ v Φορτούνα Ντίσελντορφ":{},"Σάλκε v Χέρτα Βερολίνου":{}},"Γαλλία - Ligue 1":{"Λιλ v Γκινγκάμπ":{"1":"2.00","2":"3.90","X":"3.40"},"Μπορντό v Μονακό":{"1":"3.10","2":"2.20","X":"3.60"},"Μαρσέιγ v Ρεν":{"1":"1.50","2":"6.50","X":"4.33"}}};
			do {
				this.matches = this.getRandom(lastMatches);
				if(lastMatches) {
					var changes = this.calculateChanges(this.matches, lastMatches)
					if(changes.length > 0) {
						this.events.emit('change', changes);
					}
				}
				lastMatches = this.matches;
				await sleep(100);
			} while (true);
			return;
		}
		var options = {
			args : [
				"--disable-setuid-sandbox",
				"--no-sandbox",
				//'--proxy-server=socks5://127.0.0.1:1337',
			],
			headless: false, // launch headful mode
/*			slowMo: 250, // slow down puppeteer script so that it's easier to follow visually
			devtools: true,*/
			delay: 50,
			ignoreHTTPSErrors: true,
		}
		if(debug) {
			options = {
				args : [
					"--disable-setuid-sandbox",
					"--no-sandbox",
					//'--proxy-server=socks5://127.0.0.1:1337',
				],
				headless: false, // launch headful mode
				slowMo: 250, // slow down puppeteer script so that it's easier to follow visually
				devtools: true,
				delay: 50,
				ignoreHTTPSErrors: true,
			}
			this.delay = 1000;
		}
		var browser = await puppeteer.launch(options);
		const page = await browser.newPage();
		await page.goto('https://www.' + 'bet' + '365'+'.gr/#/HO/');
		try {
			var status = 0;
			status = await this.clickByText(page, `Εκτενής Γκάμα Αθλημάτων`);
			status = await this.clickByText(page, `Ποδόσφαιρο`);
			await page.waitFor(2*this.delay);


			status = await this.clickByText(page, `Σημερινοί Αγώνες`);
			if(status==-1) {
				await this.clickByText(page, `Κύριες αγορές`);
			}
			await page.waitFor(2*this.delay);
			await page.addScriptTag({path: __dirname + '/node_modules/jquery/dist/jquery.min.js'})
			await page.addScriptTag({path: __dirname + '/injected.js'})
			await page.waitFor(1*this.delay);
		}
		catch(exp) {

		}

		var lastMatches = null
		
	  
		do {
			try {
				this.matches = await page.evaluate(() => {
					return getMatches()
				});
			} catch(exp) {
				await page.addScriptTag({path: __dirname + '/node_modules/jquery/dist/jquery.min.js'})
				await page.addScriptTag({path: __dirname + '/injected.js'})
				await page.waitFor(1*this.delay);
				continue;
			}
			if(lastMatches) {
				var changes = this.calculateChanges(this.matches, lastMatches)
				if(changes.length > 0) {
					this.events.emit('change', changes);
				}
			}
		  lastMatches = this.matches;
		  
		  await page.waitFor(2*this.delay);
		} while (browser);
	},
	stop: async function() {
		if(browser)
		{
			await browser.close();
		}
	},
	getMatches : function(){
		return this.matches;
	}
	
}
