import { Component, OnInit, Input, ViewChild  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BetService } from '../bet.service';


@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss'],
  inputs : ["matches"]
})
export class MatchesComponent implements OnInit {

  @Input()
  matches : any = null;
  temp = [];
  showLeague : boolean = false;
  columns = [
    { prop: 'match' },
    { name: 'league' },
    { name: 'x' }
  ];
  @ViewChild('myTable') table: any;
  constructor(private route: ActivatedRoute,private _betService : BetService) { 

  }

  ngOnInit() {
    if(this.matches==null) {
      this._betService.getMatchesObservable({}).subscribe(message => {
        if(message) {
          this.temp = message;// [...message]
        }
        if(this.matches == null) {
          this.matches = this.temp;
        }
      });
      this.showLeague = true;
    }
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      for (const key in d) {
        if (d.hasOwnProperty(key)) {
          const element = d[key];
          if(typeof(element) == "string" && element.toLowerCase().indexOf(val) !== -1 || !val) {
            return true;
          }
        }
      }
      return false;
    });

    // update the rows
    this.matches = [...temp];
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
    console.log(row)
  }
  onDetailToggle(event) {
    
  }
}
