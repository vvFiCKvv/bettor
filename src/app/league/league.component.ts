import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BetService } from '../bet.service';

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.scss']
})
export class LeagueComponent implements OnInit {
  name : string;
  matches : any;
  constructor(private route: ActivatedRoute,private _betService : BetService) { }

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');
    //this.matches = this._betService.getMatches({league : this.name});
    if(this.matches==null) {
      this._betService.getMatchesObservable({league : this.name}).subscribe(message => {
        if(message) {
          this.matches = [...message];
        }
        
      });
    }
  }

}
