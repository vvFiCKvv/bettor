import { TestBed, inject } from '@angular/core/testing';

import { ApplicationStateService } from './application-state-service.service';

describe('ApplicationStateServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationStateService]
    });
  });

  it('should be created', inject([ApplicationStateService], (service: ApplicationStateService) => {
    expect(service).toBeTruthy();
  }));
});
