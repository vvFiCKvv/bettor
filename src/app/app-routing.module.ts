import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatchesComponent } from './matches/matches.component';
import { LeaguesComponent } from './leagues/leagues.component';
import { LeagueComponent } from './league/league.component';
import { MatchDetailsComponent } from './match-details/match-details.component';

const routes: Routes =  [
  /*{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },*/
  { path: 'matches', component: MatchesComponent },
  { path: 'leagues', component: LeaguesComponent },
  { path: 'league/:name', component: LeagueComponent },
  { path: 'match/:league/:match', component: MatchDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
