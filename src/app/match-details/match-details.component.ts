import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BetService } from '../bet.service';

@Component({
  selector: 'app-match-details',
  templateUrl: './match-details.component.html',
  styleUrls: ['./match-details.component.scss'],
  inputs : ["match","showHeader"]
})

export class MatchDetailsComponent implements OnInit {
  @Input()
  showHeader : boolean = true;
  @Input()
  match : any = null;
  data = [];
 options = {width: 'auto',  labels: ['Date','1','x','2'], xlabel: 'X label text', ylabel: 'Y label text', animatedZooms: true, pointSize: 4};

  constructor(private route: ActivatedRoute,private _betService : BetService) { }

  ngOnInit() {
    if(this.match == null) {
    var match = this.route.snapshot.paramMap.get('match');
    var  league = this.route.snapshot.paramMap.get('league');
     this._betService.getMatchesObservable({league : league, match : match}).subscribe(message => {
      if(message) {
        this.match = message[0];
        this.data = this.match.graphData;
      }
    });
    
    this.data = [];


    }
  }

}
