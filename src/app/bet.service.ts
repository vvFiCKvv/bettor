import { Injectable, OnInit } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class BetService implements OnInit {
  private url = 'http://localhost:3000';
  private socket;

  onMatches() {
    let observable = new Observable(observer => {
      this.socket.on('matches', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

  onUpdates() {
    let observable = new Observable(observer => {
      this.socket.on('updates', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }
  constructor() {
    this.socket = io(this.url);
    this.ngOnInit();
  }
  ngOnInit() {
    this.socket.emit('getMatches');
    this.onMatches().subscribe(message => {
      this._data = message;
      for (const leagueName in this._data) {
        if (this._data.hasOwnProperty(leagueName)) {
          const league = this._data[leagueName];
          for (const matchName in league) {
            if (league.hasOwnProperty(matchName)) {
              const match = league[matchName];
              for (const market in match) {
                if (match.hasOwnProperty(market)) {
                  const oddsValue = parseFloat(match[market]);
                  match[market] = { odds: oddsValue, time: Date() };
                }
              }
              if (match['1'] && match['X'] && match['2']) {
                match.graphData = [
                  [
                    new Date(),
                    match['1'].odds,
                    match['X'].odds,
                    match['2'].odds
                  ]
                ];
              }
              else {
                match.graphData = [];
              }
            }
          }
        }
      }
    }

    );

    this.onUpdates().subscribe(message => {
      var msgList = message['msg'];
      var time = new Date(message['time']);
      for (const msg of msgList) {
        let leagueName: string = msg['league'], matchName: string = msg['match'], marketName: string = msg['market'], oddsValue: number = msg['odds'];
        let league = null;
        let match = null;
        let market = null;
        let odds = null;
        if (this._data.hasOwnProperty(leagueName)) {
          league = this._data[leagueName];
        }
        else {
          league = {};
          this._data[leagueName] = league;;
        }
        if (league.hasOwnProperty(matchName)) {
          match = league[matchName];
        }
        else {
          match = {};
          league[matchName] = match;
        }

        if (match.hasOwnProperty(marketName)) {
          odds = match[marketName];
        }
        else {
          odds = {};
          match[marketName] = odds;
        }
        odds = { odds: oddsValue, time };
        match[marketName] = odds;
        if (match.graphData) {
          match.graphData.push([time, marketName == '1' ? oddsValue : null, marketName == 'X' ? oddsValue : null, marketName == '2' ? oddsValue : null]);
        }
        match.lastUpdated = time;
      }
    })
  }

  private _data: any = {} // {"Αγγλία - Πρέμιερ Λιγκ":{"Γουότφορντ v Κρίσταλ Πάλας":{"1":"5.25","2":"1.61","X":"4.00"},"Νιούκαστλ v Τσέλσι":{"1":"5.00","2":"1.61","X":"4.00"},"Φούλαμ v Μπέρνλι":{"1":"5.25","2":"1.70","X":"3.50"},"Μάντσεστερ Γιουν. v Τότεναμ":{},"Λέστερ v Λίβερπουλ":{},"Γουέστ Χαμ v Γουλβς":{},"Έβερτον v Χάντερσφιλντ":{},"Κρίσταλ Πάλας v Σαουθάμπτον":{},"Μπράιτον v Φούλαμ":{},"Τσέλσι v Μπόρνμουθ":{},"Μαν. Σίτι v Νιούκαστλ":{},"Κάρντιφ v Άρσεναλ":{},"Γουότφορντ v Τότεναμ":{},"Μπέρνλι v Μάντσεστερ Γιουν.":{}},"Ισπανία - Πριμέρα Ντιβιζιόν":{"Ατλέτικο Μαδρίτης1-0Ράγιο Βαγεκάνο":{"1":"3.60","2":"2.10","X":"3.25"},"Βαγιαδολίδ v Μπαρτσελόνα":{"1":"2.35","2":"3.30","X":"3.00"},"Εσπανιόλ v Βαλένθια":{"1":"1.083","2":"24.00","X":"10.00"},"Σεβίλλη v Βιγιαρεάλ":{"1":"3.00","2":"2.30","X":"3.40"},"Χιρόνα v Ρεάλ Μαδρίτης":{},"Λεβάντε v Θέλτα":{},"Αθλέτικ Μπιλμπάο v Ουέσκα":{},"Χετάφε v Βαγιαδολίδ":{},"Βιγιαρεάλ v Χιρόνα":{},"Εϊμπάρ v Ρεάλ Σοσιεδάδ":{},"Θέλτα v Ατλέτικο Μαδρίτης":{},"Ράγιο Βαγεκάνο v Αθλέτικ Μπιλμπάο":{},"Ρεάλ Μαδρίτης v Λεγκανιές":{},"Λεβάντε v Βαλένθια":{},"CD Αλαβές v Εσπανιόλ":{},"Μπαρτσελόνα v Ουέσκα":{},"Μπέτις v Σεβίλλη":{}},"Ιταλία - Σέριε Α":{"Νάπολι2-2Μίλαν":{"1":"1.57","2":"5.50","X":"4.00"},"Σπαλ v Πάρμα":{"1":"1.53","2":"6.00","X":"4.00"},"Ίντερ v Τορίνο":{"1":"2.05","2":"3.80","X":"3.20"},"Κάλιαρι v Σασουόλο":{"1":"1.22","2":"13.00","X":"5.75"},"Ουντινέζε v Σαμπντόρια":{"1":"5.50","2":"1.61","X":"3.80"},"Τζένοα v Έμπολι":{"1":"1.95","2":"3.90","X":"3.40"},"Φιορεντίνα v Κιέβο":{"1":"1.66","2":"5.25","X":"3.60"},"Φροζινόνε v Μπολόνια":{},"Ρόμα v Αταλάντα":{},"Μίλαν v Ρόμα":{},"Μπολόνια v Ίντερ":{},"Πάρμα v Γιουβέντους":{},"Φιορεντίνα v Ουντινέζε":{},"Αταλάντα v Κάλιαρι":{},"Κιέβο v Έμπολι":{},"Λάτσιο v Φροζινόνε":{},"Σαμπντόρια v Νάπολι":{},"Σασουόλο v Τζένοα":{},"Τορίνο v Σπαλ":{}},"Γερμανία - Μπουντεσλίγκα Ι":{"Μάιντζ v Στουτγκάρδη":{"1":"1.44","2":"7.50","X":"4.50"},"Ντόρτμουντ v RB Λέιπζιγκ":{"1":"1.75","2":"4.75","X":"3.50"},"Αννόβερο v Ντόρτμουντ":{},"TSG Χόφενχαϊμ v Φράιμπουργκ":{},"Άιντραχτ Φρανκφούρτης v Βέρντερ Βρέμης":{},"Άουγκσμπουργκ v Γκλάντμπαχ":{},"Λεβερκούζεν v Βόλφσμπουργκ":{},"Νυρεμβέργη v Μάιντζ":{},"Στουτγκάρδη v Μπάγερν Μονάχου":{},"RB Λέιπζιγκ v Φορτούνα Ντίσελντορφ":{},"Σάλκε v Χέρτα Βερολίνου":{}},"Γαλλία - Ligue 1":{"Λιλ v Γκινγκάμπ":{"1":"2.00","2":"3.90","X":"3.40"},"Μπορντό v Μονακό":{"1":"3.10","2":"2.20","X":"3.60"},"Μαρσέιγ v Ρεν":{"1":"1.50","2":"6.50","X":"4.33"}}};
  getLeagues(): any {
    var ret: any[] = [];
    for (const leagueName in this._data) {
      if (this._data.hasOwnProperty(leagueName)) {
        const matches = this._data[leagueName];
        ret.push({ league: leagueName, matches: Object.keys(matches).length });
      }
    }
    return ret;
  }
  getMatchesObservable(filters: { league?: string, match?: string }): Observable<any> {
    return new Observable(observer => {
      setInterval(() => {
        var data = this.getMatches(filters);
        observer.next(data);
      }, 1000);
    });
  }
  getMatches(filters: { league?: string, match?: string }): any {
    var ret: any[] = [];
    for (const leagueName in this._data) {
      if (this._data.hasOwnProperty(leagueName)) {
        const league = this._data[leagueName];
        if (filters.league && filters.league != leagueName) {
          continue;
        }
        for (const matchName in league) {
          if (league.hasOwnProperty(matchName)) {
            if (filters.match && filters.match != matchName) {
              continue;
            }
            const odds = league[matchName];
            const odds2 = odds[matchName];
            /*
            let oddsTable : string[] = [];
            let marketTable : string[] =  [];
            for (const market in odds) {
              if (odds.hasOwnProperty(market)) {
                const value : string = odds[market];
                marketTable.push(value);
                oddsTable.push(value);
              }
            }
            */
            ret.push({ league: leagueName, name: matchName, ...odds })
          }
        }
      }
    }
    return ret;
  }
}
