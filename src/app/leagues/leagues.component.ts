import { Component, OnInit ,ViewChild} from '@angular/core';
import { BetService } from '../bet.service';

@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.scss']
})
export class LeaguesComponent implements OnInit {

  leagues : any[];
  temp : any[];
  @ViewChild('myTable') table: any;
  constructor(private _betService :BetService) { 

  }

  ngOnInit() {
    this.leagues = this._betService.getLeagues();
    this.temp = this.leagues;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      for (const key in d) {
        if (d.hasOwnProperty(key)) {
          const element = d[key];
          if(element.toLowerCase().indexOf(val) !== -1 || !val) {
            return true;
          }
        }
      }
      return false;
    });

    // update the rows
    this.leagues = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

}
