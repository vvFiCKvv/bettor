import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';


import {BetService} from './bet.service'
import { ApplicationStateService } from './application-state-service.service';
import { MatchesComponent } from './matches/matches.component';
import { LeaguesComponent } from './leagues/leagues.component';
import { LeagueComponent } from './league/league.component';
import { MatchDetailsComponent } from './match-details/match-details.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { NgDygraphsModule } from 'ng-dygraphs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MyNavComponent } from './my-nav/my-nav.component';

import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    MatchesComponent,
    LeaguesComponent,
    LeagueComponent,
    MatchDetailsComponent,
    MyNavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgxDatatableModule,
    NgDygraphsModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  providers: [BetService,ApplicationStateService],
  bootstrap: [AppComponent]
})
export class AppModule { }

/*
{"Αγγλία - Πρέμιερ Λιγκ":{"Γουότφορντ v Κρίσταλ Πάλας":{"1":"5.25","2":"1.61","X":"4.00"},"Νιούκαστλ v Τσέλσι":{"1":"5.00","2":"1.61","X":"4.00"},"Φούλαμ v Μπέρνλι":{"1":"5.25","2":"1.70","X":"3.50"},"Μάντσεστερ Γιουν. v Τότεναμ":{},"Λέστερ v Λίβερπουλ":{},"Γουέστ Χαμ v Γουλβς":{},"Έβερτον v Χάντερσφιλντ":{},"Κρίσταλ Πάλας v Σαουθάμπτον":{},"Μπράιτον v Φούλαμ":{},"Τσέλσι v Μπόρνμουθ":{},"Μαν. Σίτι v Νιούκαστλ":{},"Κάρντιφ v Άρσεναλ":{},"Γουότφορντ v Τότεναμ":{},"Μπέρνλι v Μάντσεστερ Γιουν.":{}},"Ισπανία - Πριμέρα Ντιβιζιόν":{"Ατλέτικο Μαδρίτης1-0Ράγιο Βαγεκάνο":{"1":"3.60","2":"2.10","X":"3.25"},"Βαγιαδολίδ v Μπαρτσελόνα":{"1":"2.35","2":"3.30","X":"3.00"},"Εσπανιόλ v Βαλένθια":{"1":"1.083","2":"24.00","X":"10.00"},"Σεβίλλη v Βιγιαρεάλ":{"1":"3.00","2":"2.30","X":"3.40"},"Χιρόνα v Ρεάλ Μαδρίτης":{},"Λεβάντε v Θέλτα":{},"Αθλέτικ Μπιλμπάο v Ουέσκα":{},"Χετάφε v Βαγιαδολίδ":{},"Βιγιαρεάλ v Χιρόνα":{},"Εϊμπάρ v Ρεάλ Σοσιεδάδ":{},"Θέλτα v Ατλέτικο Μαδρίτης":{},"Ράγιο Βαγεκάνο v Αθλέτικ Μπιλμπάο":{},"Ρεάλ Μαδρίτης v Λεγκανιές":{},"Λεβάντε v Βαλένθια":{},"CD Αλαβές v Εσπανιόλ":{},"Μπαρτσελόνα v Ουέσκα":{},"Μπέτις v Σεβίλλη":{}},"Ιταλία - Σέριε Α":{"Νάπολι2-2Μίλαν":{"1":"1.57","2":"5.50","X":"4.00"},"Σπαλ v Πάρμα":{"1":"1.53","2":"6.00","X":"4.00"},"Ίντερ v Τορίνο":{"1":"2.05","2":"3.80","X":"3.20"},"Κάλιαρι v Σασουόλο":{"1":"1.22","2":"13.00","X":"5.75"},"Ουντινέζε v Σαμπντόρια":{"1":"5.50","2":"1.61","X":"3.80"},"Τζένοα v Έμπολι":{"1":"1.95","2":"3.90","X":"3.40"},"Φιορεντίνα v Κιέβο":{"1":"1.66","2":"5.25","X":"3.60"},"Φροζινόνε v Μπολόνια":{},"Ρόμα v Αταλάντα":{},"Μίλαν v Ρόμα":{},"Μπολόνια v Ίντερ":{},"Πάρμα v Γιουβέντους":{},"Φιορεντίνα v Ουντινέζε":{},"Αταλάντα v Κάλιαρι":{},"Κιέβο v Έμπολι":{},"Λάτσιο v Φροζινόνε":{},"Σαμπντόρια v Νάπολι":{},"Σασουόλο v Τζένοα":{},"Τορίνο v Σπαλ":{}},"Γερμανία - Μπουντεσλίγκα Ι":{"Μάιντζ v Στουτγκάρδη":{"1":"1.44","2":"7.50","X":"4.50"},"Ντόρτμουντ v RB Λέιπζιγκ":{"1":"1.75","2":"4.75","X":"3.50"},"Αννόβερο v Ντόρτμουντ":{},"TSG Χόφενχαϊμ v Φράιμπουργκ":{},"Άιντραχτ Φρανκφούρτης v Βέρντερ Βρέμης":{},"Άουγκσμπουργκ v Γκλάντμπαχ":{},"Λεβερκούζεν v Βόλφσμπουργκ":{},"Νυρεμβέργη v Μάιντζ":{},"Στουτγκάρδη v Μπάγερν Μονάχου":{},"RB Λέιπζιγκ v Φορτούνα Ντίσελντορφ":{},"Σάλκε v Χέρτα Βερολίνου":{}},"Γαλλία - Ligue 1":{"Λιλ v Γκινγκάμπ":{"1":"2.00","2":"3.90","X":"3.40"},"Μπορντό v Μονακό":{"1":"3.10","2":"2.20","X":"3.60"},"Μαρσέιγ v Ρεν":{"1":"1.50","2":"6.50","X":"4.33"}}}
*/