# Bettor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.

## Installation
 Install [Git/GitExtensions](https://github.com/gitextensions/gitextensions/releases).
 
 Run `git clone https://vvFiCKvv@bitbucket.org/vvFiCKvv/bettor.git`
 
 Then `cd bettor`
 
 Install [NodeJs](https://nodejs.org/en/download/)
 
 Run `npm install` in both root directory and `/server` directory

## Start web server and backend
 
 Run `node run start` to run web server and backend.
 
 Run `node run start:debug` to show browser, it will scrap all data it sees.
 
 Run `node run start:random` to test web server with dammy data from backend.
 
 Run `node run start:prod` buld the project and run light-server so it can be work offline (pwa / serviceworker).

## Angular Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

